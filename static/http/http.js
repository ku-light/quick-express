const baseUrl = 'http://192.168.0.101:8080/quick/';
const http = (url, method, data) => {
  return new Promise((resolve, reject) => {
    //wx.showLoading();
    var header = Object.assign({},{'source':'love'})
    if(wx.getStorageSync('token').length>0){
      header = Object.assign(header,{'X-Token': wx.getStorageSync('token')})
    }
    if (method === 'GET') {
      header = Object.assign(header,{'content-type': "application/x-www-form-urlencoded"})
    } else if (method === 'POST') {
      header = Object.assign(header,{'content-type': 'application/json'})
    }
    wx.request({
      url: baseUrl + url,
      data:data,
      method:method,
      header: header,
      timeout: 6000,
      success: (res) => {
        setTimeout(()=>{
          wx.hideLoading();
        },500);
        if (res.statusCode === 500) {
          wx.showModal({
            title: '提示',
            content: '网络服务异常！',
            showCancel: false
          })
          reject(res);
        } else if (res.statusCode === 200) {
          const {code,message} = res.data
          if (code === 200) {
            resolve(res.data.result);
          }else if(code === 20001 || code === 20003){
              wx.navigateTo({
                url: '/pages/login/index',
              })
          } else {
            reject(res);
          }
        } else {
          wx.showModal({
            title: '错误信息',
            content: '操作失败！如需帮助请联系技术人员',
            showCancel: false
          })
        }
      },
      fail: (err) => {
        wx.hideLoading();
        wx.showModal({
          title: '错误信息',
          content: '网络不可用，请检查你的网络状态或稍后再试！',
          showCancel: false
        })
        reject(err);
      }
    })
  })
}
const post =(url,params)=>{
   return http(url,'POST',params)
}
const get = (url)=>{
   return http(url,'GET')
}
export {
  post,
  get
}
