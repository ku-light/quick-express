import {post,get} from './http'


const api = {
  login(params){
    return post('api/wx/wxLogin',params)
  },
  checkSession(code){
    return get('api/wx/auth?code='+code)
  },
  getUser(){
    return get('api/wx/getUser')
  },
  updateInfor(params){
    return post('api/wx/wxUpdateInfor',params)
  },
  loadBanner(params){
    return post('api/banner/search',params)
  },
  loadArticle(){
    return post('api/article/search',{})
  },
  loadGoods(){
    return post('api/shop/goods/search',{})
  },
  loadGoodsView(id){
    return get('api/shop/goods/queryId/'+id)
  },
  getAccessToken(){
    return get('https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wx1ef9cf1275916c79&secret=69702c43670cabd503fd7ac4b624bf17')
  }
}

export {
  api
}