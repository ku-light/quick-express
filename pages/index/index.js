import { api } from "../../static/http/api"

// index.js
const defaultAvatarUrl = 'https://mmbiz.qpic.cn/mmbiz/icTdbqWNOwNRna42FI242Lcia07jQodd2FJGIYQfG0LAJGFxM4FbnQP6yfMxBgJ0F3YRqJCJ1aPAK2dQagdusBZg/0'

Page({
  data: {
    bannerBack:'',
    banners:[],
    lists:[],
    goods:[],
    products:[],
    triggered:false,
    isHeader:false
  },
  onReady(){
    const that = this
    that.loadData()
  },
  handleView(e){
    const {id} = e.currentTarget.dataset.good;
    wx.navigateTo({
      url: '/pages/views/index?id='+id
    })
  },
  loadData(){
    const that = this
    api.loadBanner({search:{site:'00'}}).then(res=>{
      that.setData({banners:res.list,bannerBack:res.list[0].backColor})
    })
    api.loadBanner({search:{site:'01'}}).then(res=>{
      that.setData({products:res.list})
    })
    // api.loadArticle().then(res=>{
    //   that.setData({lists:res.list})
    //   wx.stopPullDownRefresh();
    // })
    api.loadGoods().then(res=>{
      that.setData({goods:res.list})
      that.setData({triggered:false})
    })
  },
  loadMore(){
    console.log("123123")
  },
  onPullDownRefresh() {
    const that = this
    that.loadData()
  },
  bindViewTap() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onScroll(e){
    const that = this
    if(e.detail.scrollTop>=160){
        that.setData({isHeader:true})
    }else{
      that.setData({isHeader:false})
    }
  },
  onChangeSwiper(e){
    const that = this
    that.setData({bannerBack:that.data.banners[e.detail.current].backColor})
  },
  onChooseAvatar(e) {
    const { avatarUrl } = e.detail
    const { nickName } = this.data.userInfo
    this.setData({
      "userInfo.avatarUrl": avatarUrl,
      hasUserInfo: nickName && avatarUrl && avatarUrl !== defaultAvatarUrl,
    })
  },
  onInputChange(e) {
    const nickName = e.detail.value
    const { avatarUrl } = this.data.userInfo
    this.setData({
      "userInfo.nickName": nickName,
      hasUserInfo: nickName && avatarUrl && avatarUrl !== defaultAvatarUrl,
    })
  },
  getUserProfile(e) {
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认，开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log(res)
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    })
  },
})
