// pages/backImage/index.js
import { api } from "../../static/http/api"
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
      this.loadData()
  },
  loadData(){
    const that = this;
    api.getUser().then(res=>{
      that.setData({user:res})
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },
  handlePicture(e){
    const that = this;
    wx.chooseMedia({
        count:1,
        mediaType:['image'],
        sourceType:"album",
        sizeType:['compressed'],
        maxDuration:30,
        success:(res)=>{
          const avatarUrl = res.tempFiles[0].tempFilePath
          wx.uploadFile({
            filePath: avatarUrl,
            name: 'file',
            url: 'http://192.168.0.101:8080/quick/api/ftp/upload',
            header:{
              'X-token':wx.getStorageSync('token'),
              'source':'love'
            },
            success:(res)=>{
              const {url} = JSON.parse(res.data).result
              const userInfor = that.data.user
              that.setData({user: Object.assign(userInfor,{backImgUrl:url})});
              api.updateInfor(that.data.user).then(res=>{
               that.loadData()
              })
            }
          })
        }
    })
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})