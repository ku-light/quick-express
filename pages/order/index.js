import { api } from "../../static/http/api"

// pages/order/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    active:0,
    tabs:['待支付','已付款','待发货','已签收','已退款/退货'],
    triggered:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    this.loadData()
  },
  loadData(){
    const that = this
    that.setData({triggered:true})
    api.loadGoods().then(res=>{
      that.setData({goods:res.list})
      that.setData({triggered:false})
    })
  },
  handleTabs(event){
    const that = this
    const {index} = event.currentTarget.dataset
    that.setData({active:index})
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.loadData()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
      console.log("onPullDownRefresh")
  },

  /**
   * 页面上拉触底事件的处理函数
   
  onReachBottom() {

  },
*/
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})