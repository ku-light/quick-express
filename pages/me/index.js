import { api } from "../../static/http/api"

// pages/me/index.js
Page({
  data: {
    background:'http://192.168.0.104:8000/2024/04/06/1712405879436-85c8603b-aa1d-4ee4-a4ae-1e0f201530a2.jpg',
    triggered:false,
    isHeader:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.loadData()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    this.loadData()
  },
  loadData(){
    const that = this;
    api.getUser().then(res=>{
      that.setData({user:res,triggered:false})
    })
  },
  handleSeting(){
    wx.navigateTo({
      url: '/pages/seting/index',
    })
  },
  onScroll(e){
    const that = this
    if(e.detail.scrollTop>=160){
        that.setData({isHeader:true})
    }else{
      that.setData({isHeader:false})
    }
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    const that = this;
    that.loadData()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    const that = this
    that.loadData()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
      console.log("触底")
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})