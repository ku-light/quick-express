// custom-tab-bar/index.js
import {api} from '../../api/index'
Component({
    properties: {
        active:{
            type: String,
            value: '0'
        }
    },
    data: {
          tabs:[]
    },
    ready(){
        console.log(this.properties.active)
    },
    pageLifetimes:{

    },
    /**
     * 组件的方法列表
     */
    methods: {
      switchTab(e) {
          if(e.currentTarget.dataset.id === "0"){
            wx.switchTab({
                url: '/pages/index/index',
                success:(res)=>{
                  let page = getCurrentPages().pop();  
                  page.onReady(); 
                }
            })
        }
        if(e.currentTarget.dataset.id === "1"){
            wx.switchTab({
              url: '/pages/works/index',
              success:(res)=>{
                let page = getCurrentPages().pop();  
                page.onReady(); 
              }
            })
        }
        if(e.currentTarget.dataset.id === "2"){
            wx.switchTab({
                url: '/pages/me/index',
                success:function(res){
                  let page = getCurrentPages().pop();
                  page.onReady();
                }
            })
        }
      }
    }
  })